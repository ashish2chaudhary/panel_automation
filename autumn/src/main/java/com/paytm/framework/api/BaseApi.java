package com.paytm.framework.api;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.paytm.framework.api.curlloggingutil.CurlLoggingRestAssuredConfigBuilder;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.fest.assertions.api.Assertions;
import org.testng.Reporter;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static io.restassured.RestAssured.given;

public class BaseApi {

    private RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
    private MethodType method;

    public enum MethodType {
        POST, GET, PUT, DELETE, PATCH
    }

    public MethodType getMethod() {
        return method;
    }

    public void setMethod(MethodType method) {
        this.method = method;
    }

    public RequestSpecBuilder getRequestSpecBuilder() {
        return requestSpecBuilder;
    }

    public Response execute() {
        RequestSpecification requestSpecification = requestSpecBuilder
                .addFilter(new RequestLoggingFilter())
                .addFilter(new ResponseLoggingFilter()).build();
        Response response;
        RestAssured.defaultParser = Parser.JSON;
        RestAssuredConfig config = new CurlLoggingRestAssuredConfigBuilder().build();
        switch (method) {
            case GET:
                response = given().config(config).spec(requestSpecification).when().get();
                break;
            case POST:
                response = given().config(config).spec(requestSpecification).when().post();
                break;
            case PUT:
                response = given().config(config).spec(requestSpecification).when().put();
                break;
            case DELETE:
                response = given().config(config).spec(requestSpecification).when().delete();
                break;
            case PATCH:
                response = given().config(config).spec(requestSpecification).when().patch();
                break;
            default:
                throw new RuntimeException("API method not specified");

        }
        printResponse(response);
        return response;
    }

    public Response executeUntilExpectedConditionMet(String expectedJSONKey, String expectedJSONValue, int pollingTimeInSec, int pollingCountInSec) {
        Response response = null;
        String actualJSONValue = "";
        try {
            for (int i = 0; i < pollingCountInSec; i++) {
                response = this.execute();
                actualJSONValue = response.jsonPath().get(expectedJSONKey).toString();
                if (expectedJSONValue.equalsIgnoreCase(actualJSONValue)) {
                    break;
                }
                Thread.sleep(pollingTimeInSec * 1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assertions.assertThat(actualJSONValue).isEqualToIgnoringCase(expectedJSONValue);
        return response;
    }

    private void printResponse(Response response) {
        String contentType = response.contentType();

        if (contentType.toLowerCase().contains("text/html")) {
            final DateFormat timeFormat = new SimpleDateFormat("MM.dd.yyyy HH-mm-ss");
            final String fileName = Reporter.getCurrentTestResult().getMethod().getMethodName() + "_" + timeFormat.format(new Date()) + ".html";

            String outputDir = Reporter.getCurrentTestResult().getTestContext().getOutputDirectory();
            outputDir = outputDir.substring(0, outputDir.lastIndexOf(File.separator)) + "/html";

            File file = new File(outputDir + File.separator + fileName);
            if (file.getParentFile() != null) {
                file.getParentFile().mkdirs();
            }
            PrintWriter writer = null;

            try {
                file.createNewFile();
                writer = new PrintWriter(file);
                writer.write(response.asString());
                writer.flush();
                Reporter.log("<a href=\"" + fileName + "\" target=\"_blank\"><b>API Response</b></a><br>");
            } catch (Throwable e) {
                throw new RuntimeException(e);
            } finally {
                writer.close();
            }
        } else {
            Reporter.log("<br>" + "API Response:" + response.getBody().prettyPrint());
        }
    }

    public BaseApi setBodyOmitNullValueAttributes(Object obj) {
        String request = "";
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.USE_ANNOTATIONS, false);
        mapper.setSerializationInclusion(Include.NON_NULL);
        try {
            request = mapper.writeValueAsString(obj);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        requestSpecBuilder.setBody(request);
        return this;
    }

    public BaseApi setBodyKeepNullValueAttributes(Object obj) {
        String request = "";
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.USE_ANNOTATIONS, false);
        mapper.setSerializationInclusion(Include.ALWAYS);
        try {
            request = mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        requestSpecBuilder.setBody(request);
        return this;
    }

    public BaseApi setBody(Object obj) {
        requestSpecBuilder.setBody(obj);
        return this;
    }

}
