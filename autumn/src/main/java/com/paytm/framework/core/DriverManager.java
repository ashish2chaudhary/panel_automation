package com.paytm.framework.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class creates and manages the ThreadLocal instances of webDriverThreadPool, webDriverPageWait, webDriverElementWait,
 * captureScreenShot, platformName, browserName and headless. 
 *
 */
public class DriverManager {

    private static List<WebDriverThread> webDriverThreadPool =
            Collections.synchronizedList(new ArrayList<WebDriverThread>());

    private static ThreadLocal<WebDriverThread> driverThread = new ThreadLocal<WebDriverThread>(){
        @Override
        protected WebDriverThread initialValue(){
            WebDriverThread webDriverThread = new WebDriverThread();
            webDriverThreadPool.add(webDriverThread);
            return webDriverThread;
        }
    };

    private static final ThreadLocal<WebDriverWait> webDriverPageWait = new ThreadLocal<WebDriverWait>(){
        @Override
        protected WebDriverWait initialValue(){
            WebDriverWait wait = new WebDriverWait(getDriver(), ExecutionConfig.MAX_PAGE_LOAD_WAIT_TIME);
            return wait;
        }
    };

    private static ThreadLocal<WebDriverWait> webDriverElementWait = new ThreadLocal<WebDriverWait>(){
        @Override
        protected WebDriverWait initialValue(){
            WebDriverWait wait = new WebDriverWait(getDriver(), ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME);
            return wait;
        }
    };

    private static ThreadLocal<Boolean> captureScreenShot = new ThreadLocal<Boolean>(){
        @Override
        protected Boolean initialValue(){
            return true;
        }
    };

    private static ThreadLocal<String> platformName = new ThreadLocal<String>();
    private static ThreadLocal<String> browserName = new ThreadLocal<String>();
    private static ThreadLocal<Boolean> headless = new ThreadLocal<Boolean>();

    /**
     * This method is used to get the WebDriver instance.
     *
     * @return WebDriver instance.
     */
    public static WebDriver getDriver(){
        WebDriverThread webDriverThread = driverThread.get();
        if (webDriverThread.getWebDriver() != null) {
            if (!webDriverThread.getBrowser().equalsIgnoreCase(DriverManager.getBrowserName()) ||
                    !webDriverThread.getPlatform().equalsIgnoreCase(DriverManager.getPlatformName())) {
                webDriverThread.quitDriver();
            }
        }

        return driverThread.get().getDriver();
    }

    /**
     * This method is used to get the WebDriver's current instance.
     *
     * @return WebDriver' current instance.
     */
    public static WebDriver getCurrentWebDriver(){
        return driverThread.get().getWebDriver();
    }

    /**
     * This method is used to get the platform.
     *
     * @return platform.
     */
    public static String getPlatformName() {
        return platformName.get();
    }

    /**
     * This method is used to set the platform.
     *
     * @param platform.
     */
    public static void setPlatformName(String platform) {
        platformName.set(platform);
    }

    /**
     * This method is used to get the browser.
     *
     * @return browser.
     */
    public static String getBrowserName() {
        return browserName.get();
    }

    /**
     * This method is used to set the browser.
     *
     * @param browser.
     */
    public static void setBrowserName(String browser) {
        browserName.set(browser);
    }
    
    /**
     * This method is used to get if the browser is headless or not.
     *
     * @return boolean.
     */
    public static Boolean isHeadless() {
        return headless.get();
    }

    /**
     * This method is used to set the browser as headless.
     *
     * @param boolean.
     */
    public static void setHeadless(String headlessValue) {
        if(headlessValue.equalsIgnoreCase("True")) {
        	headless.set(true);
        }
        else {
        	headless.set(false);
        }
    }

    /**
     * This method is used to get if the capturescreenshot is on or not.
     *
     * @return boolean.
     */
    public static Boolean getCaptureScreenShot(){
        return captureScreenShot.get();
    }

    /**
     * This method is used to set the capturescreenshot as on or off.
     *
     * @param boolean.
     */
    public static void setCaptureScreenShot(Boolean captureScreenShot){
        DriverManager.captureScreenShot.set(captureScreenShot);
    }

    /**
     * This method is used to get the webdriver page wait.
     *
     * @return WebDriverWait.
     */
    public static WebDriverWait getWebDriverPageWait(){
        return webDriverPageWait.get();
    }

    /**
     * This method is used to set the webdriver page wait.
     *
     * @param WebDriverWait in seconds.
     */
    public static void setWebDriverPageWait(int seconds){
        webDriverPageWait.set(new WebDriverWait(getDriver(), seconds));
    }

    /**
     * This method is used to get the webdriver element wait.
     *
     * @return WebDriverWait.
     */
    public static WebDriverWait getWebDriverElementWait(){
        return webDriverElementWait.get();
    }

    /**
     * This method is used to set the webdriver element wait.
     *
     * @param WebDriverWait in seconds.
     */
    public static void setWebDriverElementWait(int seconds){
        webDriverElementWait.set(new WebDriverWait(getDriver(), seconds));
    }

    /**
     * This method is used to reset the webdriver page wait.
     *
     */
    public static void resetWebDriverPageWait(){
        if(driverThread.get().getWebDriver() != null)
            webDriverPageWait.set(new WebDriverWait(getDriver(), ExecutionConfig.MAX_PAGE_LOAD_WAIT_TIME));
    }

    /**
     * This method is used to reset the webdriver element wait.
     *
     */
    public static void resetWebDriverElementWait(){
        if(driverThread.get().getWebDriver() != null)
            webDriverElementWait.set(new WebDriverWait(getDriver(), ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME));
    }

    /**
     * This method is used to close webdriver.
     *
     */
    public static void closeDriverObjects(){
        for(WebDriverThread webDriverThread: webDriverThreadPool){
            webDriverThread.quitDriver();
        }
    }
}