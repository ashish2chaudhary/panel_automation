package com.paytm.framework.ui.element;

import com.paytm.framework.core.DriverManager;
import com.paytm.framework.core.ExecutionConfig;
import com.paytm.framework.ui.MoreExpectedConditions;

import net.bytebuddy.agent.builder.AgentBuilder.FallbackStrategy;

import org.fest.assertions.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.internal.WrapsElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

public class UIElement implements  WebElement, WrapsElement, Locatable {

    private String elementName;
    private String pageName;
    private By by;
    private WebElement webElement;
    private List<WebElement> webElementList;
    JavascriptExecutor js;
    WebDriver driver;
    private Boolean found = false;

    @Deprecated
    public UIElement(By by, String pageName) {
        this.by = by;
        this.pageName = pageName;
        this.elementName = Thread.currentThread().getStackTrace()[3].getMethodName(); //returns the method name where the element is getting initialized;
        this.driver = DriverManager.getDriver();
        js = (JavascriptExecutor) driver;
    }

    public UIElement(By by, String pageName, String elementName) {
        this.by = by;
        this.pageName = pageName;
        this.elementName = elementName;
        this.driver = DriverManager.getDriver();
        js = (JavascriptExecutor) driver;
    }

    public String getPageName() {
        return this.pageName;
    }

    public String getElementName() {
        return this.elementName;
    }

    public By getBy() {
        return this.by;
    }

    public void click() {
    	String stackTrace = "Unable to click";
        Reporter.log("<br>Click [" + elementName + "] on [" + pageName + "]");
        long oneMinuteTick = System.currentTimeMillis() + ExecutionConfig.MAX_ELEMENT_LOAD_WAIT_TIME*1000;
		while(System.currentTimeMillis() <= oneMinuteTick) { 
		    try {		    	
		    	getWrappedElement().click();
		    	found = true;
		    	break;
		    	} catch (WebDriverException e) {
		    		StringWriter sw = new StringWriter();
		    	    PrintWriter pw = new PrintWriter(sw);
		    	    e.printStackTrace(pw);
		    	    pw.flush();
		    	    stackTrace = sw.toString();
		    	} 		  
		    }
		if(!found) {
			Reporter.log("[" + elementName + "] on [" + pageName + "] is either not present or not clickable");
			throw new WebDriverException(stackTrace);
			}
		}   
    

    private void highlightElement(WebElement element, int duration) throws InterruptedException {
        String original_style = element.getAttribute("style");
        js.executeScript(
                "arguments[0].setAttribute(arguments[1], arguments[2])",
                element,
                "style",
                "border: 5px solid red; border-style: solid;");
        if (duration > 0) {
            Thread.sleep(duration * 500);
            js.executeScript("arguments[0].setAttribute(arguments[1], arguments[2])",
                    element,
                    "style",
                    original_style);
        }
    }


    public void sendKeys(CharSequence... keysToSend) {
        Reporter.log("<br>Enter text [" + Arrays.toString(keysToSend) + "] in [" + elementName + "] on [" + pageName + "]");
        getWrappedElement().sendKeys(keysToSend);
    }

    public Point getLocation() {
        return getWrappedElement().getLocation();
    }

    public void submit() {
        Reporter.log("<br>Click [" + elementName + "] on [" + pageName + "] to submit");
        getWrappedElement().submit();
    }

    public String getAttribute(String name) {
        return getWrappedElement().getAttribute(name);
    }

    public String getCssValue(String propertyName) {
        return getWrappedElement().getCssValue(propertyName);
    }

    public Dimension getSize() {
        return getWrappedElement().getSize();
    }

    @Override
    public Rectangle getRect() {
        return null;
    }

    public List<WebElement> findElements(By by) {
        return getWrappedElement().findElements(by);
    }

    public String getText() {
        return getWrappedElement().getText();
    }

    public String getTagName() {
        return getWrappedElement().getTagName();
    }

    public boolean isSelected() {
        return getWrappedElement().isSelected();
    }

    public WebElement findElement(By by) {
        return getWrappedElement().findElement(by);
    }

    public boolean isEnabled() {
        return getWrappedElement().isEnabled();
    }

    public boolean isDisplayed() {
        WebElement element;
        try {
            element = getWrappedElement();
        } catch (NoSuchElementException e) {
            return false;
        }
        return element.isDisplayed();
    }

    public void clear() {
        Reporter.log("<br>Clear text in [" + elementName + "] on [" + pageName + "]");
        getWrappedElement().clear();
    }

    public WebElement getWrappedElement() {
//        if (this.webElement == null) {
            this.webElement = DriverManager.getDriver().findElement(this.by);
//        }
        return this.webElement;
    }
    
    public List<WebElement> getWrappedElements() {
//        if (this.webElementList == null) {
            this.webElementList = DriverManager.getDriver().findElements(this.by);
//        }
        return this.webElementList;
    }

    public Coordinates getCoordinates() {
        return ((Locatable) getWrappedElement()).getCoordinates();
    }

    public boolean elementWired() {
        return (webElement != null);
    }

    public void focus() {
        Reporter.log("<br>Focus [" + elementName + "] on [" + pageName + "]");
        new Actions(DriverManager.getDriver()).moveToElement(getWrappedElement()).perform();
    }

    public void assertVisible() {
        Reporter.log("<br>Assert [" + elementName + "] is visible on [" + pageName + "]");
        Assertions.assertThat(isDisplayed()).isEqualTo(true);
    }

    public void assertNotVisible() {
        Reporter.log("<br>Assert [" + elementName + "] is not visible on [" + pageName + "]");
        Assertions.assertThat(isDisplayed()).isEqualTo(false);
    }

    public void assertText(String text) {
        Reporter.log("<br>Assert [" + elementName + "] text equals [" + text + "] on [" + pageName + "]");
        Assertions.assertThat(getText()).isEqualToIgnoringCase(text);
    }

    public void assertContainsText(String text) {
        Reporter.log("<br>Assert [" + elementName + "] contains text [" + text + "] on [" + pageName + "]");
        Assertions.assertThat(getText()).containsIgnoringCase(text);
    }

    public void assertDoesNotContainText(String text) {
        Reporter.log("<br>Assert [" + elementName + "] doesn't contain text [" + text + "] on [" + pageName + "]");
        Assertions.assertThat(getText()).doesNotContain(text);
    }

    public void assertValue(String value) {
        Reporter.log("<br>Assert [" + elementName + "] value equals [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute("value")).isEqualToIgnoringCase(value);
    }

    public void assertAttribute(String attribute, String value) {
        Reporter.log("<br>Assert [" + elementName + "] attribute [" + attribute + "] value equals [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute(attribute)).isEqualToIgnoringCase(value);
    }

    public void assertContainsValue(String value) {
        Reporter.log("<br>Assert [" + elementName + "] contains value [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute("value")).containsIgnoringCase(value);
    }

    public void assertDoesNotContainValue(String value) {
        Reporter.log("<br>Assert [" + elementName + "] doesn't contain value [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute("value")).doesNotContain(value);
    }

    private boolean isFocused() {
        return getWrappedElement().equals(DriverManager.getDriver().switchTo().activeElement());
    }
    
    public boolean isPresent() {
    	try {
			getWrappedElement();
		} catch (NoSuchElementException e) {
			return false;
		}
    	return true;
    }

    public void switchToFrame() {
        DriverManager.getDriver().switchTo().frame(getWrappedElement());
    }
    
    public void switchToParentFrame() {
        DriverManager.getDriver().switchTo().defaultContent();
    }

    public void assertIsFocused() {
        Reporter.log("<br>Assert [" + elementName + "] is focused on [" + pageName + "]");
        Assertions.assertThat(isFocused()).isEqualTo(true);
    }

    public void assertIsNotFocused() {
        Reporter.log("<br>Assert [" + elementName + "] is not focused on [" + pageName + "]");
        Assertions.assertThat(isFocused()).isEqualTo(false);
    }
    
    public void assertIsPresent() {
        Reporter.log("<br>Assert [" + elementName + "] is present on [" + pageName + "]");
        Assertions.assertThat(isPresent()).isEqualTo(true);
    }
    
    public void assertIsNotPresent() {
        Reporter.log("<br>Assert [" + elementName + "] is not present on [" + pageName + "]");
        Assertions.assertThat(isPresent()).isEqualTo(false);
    }

    public void waitUntilContainsText(String text) {
        Reporter.log("<br>Wait until [" + elementName + "] contains text [" + text + "] on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.textToBePresentInElementLocated(getBy(), text));
    }

    public void waitUntilDoesNotContainText(String text) {
        Reporter.log("<br>Wait until [" + elementName + "] contains text [" + text + "] on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.not(ExpectedConditions.textToBePresentInElementLocated(getBy(), text)));
    }

    public void waitUntilVisible() {
        Reporter.log("<br>Wait until [" + elementName + "] is visible " + "on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.visibilityOfElementLocated(getBy()));
    }

    public void waitUntilPresent() {
        Reporter.log("<br>Wait until [" + elementName + "] is present " + "on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.presenceOfElementLocated(getBy()));
    }

    public void waitUntilNotVisible() {
        Reporter.log("<br>Wait until [" + elementName + "] is not visible " + "on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.invisibilityOfElementLocated(getBy()));
    }

    public void waitUntilEditable() {
        Reporter.log("<br>Wait until [" + elementName + "] is editable " + "on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementToBeClickable(getBy()));
    }

    public void waitUntilNotEditable() {
        Reporter.log("<br>Wait until [" + elementName + "] is not editable " + "on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(getBy())));
    }

    public void waitUntilContainsAttributeValue(String attributeName, String attributeValue) {
        Reporter.log("<br>Wait until [" + elementName + "] attribute [" + attributeName + "] contains value [" + attributeValue + "] on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(MoreExpectedConditions.attributeValueToBeContainedInElement(getBy(), attributeName, attributeValue));
    }

    public void waitUntilDoesNotContainAttributeValue(String attributeName, String attributeValue) {
        Reporter.log("<br>Wait until [" + elementName + "] attribute [" + attributeName + "] does not contains value [" + attributeValue + "] on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.not(MoreExpectedConditions.attributeValueToBeContainedInElement(getBy(), attributeName, attributeValue)));
    }

    public void waitUntilContainsAttribute(String attributeName) {
        Reporter.log("<br>Wait until [" + elementName + "] contains attribute [" + attributeName + "] on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(MoreExpectedConditions.attributeToBeContainedInElement(getBy(), attributeName));
    }

    public void waitUntilDoesNotContainAttribute(String attributeName) {
        Reporter.log("<br>Wait until [" + elementName + "] does not contains attribute [" + attributeName + "] on [" + pageName + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.not(MoreExpectedConditions.attributeToBeContainedInElement(getBy(), attributeName)));
    }

    public UIElement and() {
        return this;
    }

    public void scrollToView() {
        Reporter.log("<br>Scroll [" + elementName + "] to view on [" + pageName + "]");
        String script = "arguments[0].scrollIntoView(true);";
        ((JavascriptExecutor) DriverManager.getDriver()).executeScript(script, getWrappedElement());
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return null;
    }

    public void assertDisabled() {
        Reporter.log("<br>Assert [" + elementName + " is disabled on [" + pageName + "]");
        Assertions.assertThat(isEnabled()).isFalse();
    }

    public void assertEnabled() {
        Reporter.log("<br>Assert [" + elementName + " is enabled on [" + pageName + "]");
        Assertions.assertThat(isEnabled()).isTrue();
    }
}