package com.paytm.framework.ui.element;

import com.paytm.framework.core.DriverManager;
import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

public class RadioButton extends UIElement {

    @Deprecated
    public RadioButton(By by, String pageName) {
        super(by, pageName);
    }

    public RadioButton(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public void select() {
        Reporter.log("<br>Select [" + getElementName() + "] on [" + getPageName() + "]");
        if (!isSelected()) {
            click();
        }
    }

    public boolean isSelected() {
        return getWrappedElement().isSelected();
    }

    public void assertSelected() {
        Reporter.log("<br>Assert [" + getElementName() + "] is selected on [" + getPageName() + "]");
        Assertions.assertThat(isSelected()).isEqualTo(true);
    }

    public void assertDeSelected() {
        Reporter.log("<br>Assert [" + getElementName() + "] is not selected on [" + getPageName() + "]");
        Assertions.assertThat(isSelected()).isEqualTo(false);
    }

    public void waitUntilSelected() {
        Reporter.log("<br>Wait until [" + getElementName() + "] is selected on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementSelectionStateToBe(getBy(), true));
    }

    public void waitUntilDeSelected() {
        Reporter.log("<br>Wait until [" + getElementName() + "] is not selected on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementSelectionStateToBe(getBy(), false));
    }

}