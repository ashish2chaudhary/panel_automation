package com.paytm.framework.ui.element;

import java.util.List;

import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;

public class Label extends UIElement{
	
    private String elementName;
    private String pageName;
    private By by;
    private WebElement webElement;
    private List<WebElement> webElementList;
    JavascriptExecutor js;
    WebDriver driver;
	
	@Deprecated
    public Label(By by, String pageName) {
        super(by, pageName);
    }

    public Label(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }
    
    public String getLabelText() {
    	return getText();
    }

    public void assertText(String text) {
        Reporter.log("<br>Assert [" + elementName + "] text equals [" + text + "] on [" + pageName + "]");
        Assertions.assertThat(getText()).isEqualToIgnoringCase(text);
    }

    public void assertContainsText(String text) {
        Reporter.log("<br>Assert [" + elementName + "] contains text [" + text + "] on [" + pageName + "]");
        Assertions.assertThat(getText()).containsIgnoringCase(text);
    }

    public void assertDoesNotContainText(String text) {
        Reporter.log("<br>Assert [" + elementName + "] doesn't contain text [" + text + "] on [" + pageName + "]");
        Assertions.assertThat(getText()).doesNotContain(text);
    }

    public void assertValue(String value) {
        Reporter.log("<br>Assert [" + elementName + "] value equals [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute("value")).isEqualToIgnoringCase(value);
    }

    public void assertAttribute(String attribute, String value) {
        Reporter.log("<br>Assert [" + elementName + "] value equals [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute(attribute)).isEqualToIgnoringCase(value);
    }

    public void assertContainsValue(String value) {
        Reporter.log("<br>Assert [" + elementName + "] contains value [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute("value")).containsIgnoringCase(value);
    }

    public void assertDoesNotContainValue(String value) {
        Reporter.log("<br>Assert [" + elementName + "] doesn't contain value [" + value + "] on [" + pageName + "]");
        Assertions.assertThat(getAttribute("value")).doesNotContain(value);
    }

}





