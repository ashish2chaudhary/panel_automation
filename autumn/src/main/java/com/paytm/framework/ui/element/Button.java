package com.paytm.framework.ui.element;

import com.paytm.framework.core.DriverManager;
import org.fest.assertions.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Reporter;

public class Button extends UIElement {

    @Deprecated
    public Button(By by, String pageName) {
        super(by, pageName);
    }

    public Button(By by, String pageName, String elementName) {
        super(by, pageName, elementName);
    }

    public void assertClickable() {
        Reporter.log("<br>Assert [" + getElementName() + " is clickable on [" + getPageName() + "]");
        Assertions.assertThat(isEnabled()).isEqualTo(true);
    }

    public void assertNotClickable() {
        Reporter.log("<br>Assert [" + getElementName() + " is not clickable on [" + getPageName() + "]");
        Assertions.assertThat(isEnabled()).isEqualTo(false);
    }

    public void waitUntilClickable() {
        Reporter.log("<br>Wait until [" + getElementName() + " is clickable on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.elementToBeClickable(getBy()));
    }

    public void waitUntilNotClickable() {
        Reporter.log("<br>Wait until [" + getElementName() + " is not clickable on [" + getPageName() + "]");
        DriverManager.getWebDriverElementWait().until(ExpectedConditions.not(ExpectedConditions.elementToBeClickable(getBy())));
    }
}