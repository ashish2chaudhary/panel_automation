package backofficepanel.utilities;

import java.security.InvalidParameterException;


public class Utility {
	
	public static String getDay(String date) {
		if (date.substring(0, 1).equals("0")) return date.substring(1, 2);
		else return date.substring(0, 2);
	}
	
	public static String getMonthYear(String date) {
		String month = date.substring(3, 5);
		String year = date.substring(6);
		
		switch(month) {
		case "01" : return "January " + year;
		case "02" : return "February " + year;
		case "03" : return "March " + year;
		case "04" : return "April " + year;
		case "05" : return "May " + year;
		case "06" : return "June " + year;
		case "07" : return "July " + year;
		case "08" : return "August " + year;
		case "09" : return "September " + year;
		case "10" : return "October " + year;
		case "11" : return "November " + year;
		case "12" : return "December " + year;
		default : throw new InvalidParameterException("The value of month is invalid: " +month);
		}		
	}

}
