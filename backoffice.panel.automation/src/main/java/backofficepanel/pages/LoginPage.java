package backofficepanel.pages;

import com.paytm.framework.ui.base.page.BasePage;
import com.paytm.framework.ui.element.Button;
import com.paytm.framework.ui.element.IFrame;
import com.paytm.framework.ui.element.Label;
import com.paytm.framework.ui.element.Link;
import com.paytm.framework.ui.element.TextBox;

import backofficepanel.appconstants.Constants.PagePath;
import backofficepanel.automation.LocalConfig;
import backofficepanel.utilities.ReadXML;

public class LoginPage extends BasePage{
		
	private IFrame loginViewIFrame;
	private TextBox userNameTextBox;
	private TextBox passwordTextBox;
	private Button loginButton;
		
	public LoginPage() {
		super(LoginPage.class.getSimpleName()); 
		System.out.println("Inside login page");
        this.pageURL = LocalConfig.BOP_URL + PagePath.LOGINPAGE_PATH;
        System.out.println(this.pageURL);
        userNameTextBox = new TextBox(ReadXML.getElementLocator(getPageName(), "UserName"), getPageName(), "UserName");
        passwordTextBox = new TextBox(ReadXML.getElementLocator(getPageName(), "Password"), getPageName(), "Password");
        loginButton = new Button(ReadXML.getElementLocator(getPageName(), "Login"), getPageName(), "Login");    

        launch();
    }
	
	public void switchToLoginViewIFrame() {	
		loginViewIFrame.switchToIFrame();
	}
	
	public void setUserName(String userName) {
		userNameTextBox.sendKeys(userName);
	}
	
	public void setPassword(String password) {
		passwordTextBox.sendKeys(password);
	}
	
	public void clickLoginButton() {
		loginButton.click();
	}
	
	public void waitForLoginButtonToDisappear() {
		loginButton.waitUntilNotVisible();
	}
}

