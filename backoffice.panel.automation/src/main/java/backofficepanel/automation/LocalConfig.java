package backofficepanel.automation;

import com.paytm.framework.utils.PropertyUtil;

public class LocalConfig {

    public static final String BOP_URL;

    static {
        try {
        	PropertyUtil.getInstance().load("localconfig.properties");	
        	BOP_URL = PropertyUtil.getInstance().getValue("BOP_URL");
        }
        catch (Throwable e) {
        	e.printStackTrace();
            throw new RuntimeException("Something wrong !!! Check configurations.", e);
        }
    }
}