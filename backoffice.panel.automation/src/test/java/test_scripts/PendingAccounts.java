package test_scripts;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.paytm.framework.datareader.DataProviderParams;
import com.paytm.framework.datareader.DataReaderUtil;
import com.paytm.framework.ui.base.test.BaseTest;

import backofficepanel.module.CommonModule;
import backofficepanel.module.LoginModule;

public class PendingAccounts extends BaseTest{
	private LoginModule loginModule;
	private CommonModule commonModule;
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=LoginData_Valid_Invalid" })
	@Test(description = "Performs login/Logout", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void loginTest(String userName, String password, String credentailsStatus){
		loginModule = new LoginModule();
		commonModule = new CommonModule();
		loginModule.login(userName, password);	
		System.out.println("Actual :"+commonModule.confirmLoginStatus());
		System.out.println("Expected : "+credentailsStatus);
		Assert.assertEquals(credentailsStatus, commonModule.confirmLoginStatus());
		System.out.println("Before searching");

	}	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=Search_Data" })
	@Test(description = "Verify back button on Search Result page Navigates user to Account management", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void  backButtonTest(String Searchmode, String value , String expectedresult) throws InterruptedException{
		commonModule = new CommonModule();
		System.out.println(Searchmode+"******"+value+"*****"+expectedresult);
		
		commonModule.searchBy(Searchmode, value);
		//commonModule.cancelSearch();
	}
}