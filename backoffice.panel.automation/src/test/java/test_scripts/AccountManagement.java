package test_scripts;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.paytm.framework.datareader.DataProviderParams;
import com.paytm.framework.datareader.DataReaderUtil;
import com.paytm.framework.ui.base.test.BaseTest;
import com.paytm.framework.ui.element.Button;
import com.paytm.framework.utils.CommonUtils;

import backofficepanel.module.AccountManagement_Module;
import backofficepanel.module.CommonModule;
import backofficepanel.module.LoginModule;
import backofficepanel.pages.AccountManagement_Page;
import backofficepanel.utilities.ReadXML;

public class AccountManagement extends BaseTest{
	private LoginModule loginModule;
	private CommonModule commonModule;
	private AccountManagement_Module acman_module;

	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=AccountManagement_PresenceofElement" })
	@Test(description = "Verify elements present on Account Management page", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void presenceOfElementstest(String userName, String password,String credentialsStatus , String Searchmode , String value , String username , String mobile , String emailid)
	{
		loginModule = new LoginModule();
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		loginModule.login(userName, password);	
		commonModule.navigateToAccountmanagement();
		System.out.println("Actual :"+commonModule.confirmLoginStatus());
		System.out.println("Expected : "+credentialsStatus);
		Assert.assertEquals(credentialsStatus, commonModule.confirmLoginStatus());
		commonModule.searchBy(Searchmode, value);
		acman_module.verifyAccountInfo(value,mobile,emailid);
	} 
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SAINF_AccountDetails_SavingAccount" })
	@Test(description = "Verify SAINF Account Saving Accounts  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingSainfAcc_Details(String username , String custid ,String mobile , String emailid ,
			String AC_No ,String bal , String AC_Type , String AC_Status , String branchname, 
			String branchid ,String branch_add ,String create_date , String update_date , 
			String see_deessed , String consent_stat,String NPCI_seed_date , 
			String reject , String Searchmode , String value )
	{
		System.out.println("*****************"+value);
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(Searchmode, value);
		acman_module.verifySAINFAcc_Details(username ,custid ,mobile ,emailid,
				AC_No,bal ,AC_Type,AC_Status ,branchname, branchid ,
				branch_add ,create_date  ,update_date ,see_deessed,
				consent_stat,NPCI_seed_date,reject);
	} 
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=NomineeDetails" })
	@Test(description = "Verify SAINF Account  Nominee Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingNominee_Details(String nomineename , String nominee_id , String relation , String email,
			String DOB , String aadhar , String percent , String address , String Searchmode , String value )
	{
		System.out.println("*****************"+value);
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(Searchmode, value);
		acman_module.verifyNomineeDetail( nomineename ,  nominee_id ,  relation ,  email,
			 DOB ,  aadhar ,  percent ,  address);
	}
	
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=SAINF_AccountDetails_SavingAccount" })
	@Test(description = "Verify SAIND Account Saving Accounts  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingSaindAcc_Details(String username , String custid , String mobile , String emailid ,
			String AC_No ,String bal , String AC_Type , 
			String AC_Status , String branchname, String branchid ,String branch_add ,
			String create_date , String update_date , String see_deessed , String consent_stat,
			String NPCI_seed_date , String reject , String Searchmode , String value )
	{
		System.out.println("*****************"+value);
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(Searchmode, value);
		acman_module.verifySAINFAcc_Details(username ,custid ,mobile ,emailid ,AC_No, bal , AC_Type, 
				AC_Status ,branchname, branchid ,branch_add ,create_date  ,update_date ,see_deessed,
				consent_stat,NPCI_seed_date,reject);
	} 
	
	
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=KYC_Details" })
	@Test(description = "Verify KYC  Details ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingKYCDetails(String kyc_stat , String kyc_type , String kyc_details , String pan_updated ,
			String pan_verified ,String aadhar_updated , String aadhar_verified , 
			String form_60 , String Searchmode , String value)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(Searchmode, value);
		acman_module.verifyKYC_DetailsACMANMod(kyc_stat,kyc_type,kyc_details, pan_updated ,
				pan_verified,aadhar_updated ,aadhar_verified ,form_60 );
	} 
	
	@DataProviderParams({ "fileName=InputData.csv", "tableName=Actions_Debit_Card" })
	@Test(description = "Block/Unblock PDC ", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingBlock_Unblock(String cust_id ,String debitcard_number , String alias_number , String variant
			, String qr_code , String awb , String pre_del , String delivery ,String order_id , String stat_nor ,
			String stat_tb,String stat_rb, String searchmode)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(searchmode, cust_id);
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant);  		// Verifying when status is normal
		acman_module.clickActionLink();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_tb ,variant);	
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectUnblock();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant); 
	}  
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Debit_Card" })
	@Test(description = "RestrictBlock/Unblock", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyingRestrictBlock_Unblock(String cust_id ,String debitcard_number , String alias_number , String variant
			, String qr_code , String awb , String pre_del , String delivery ,String order_id , String stat_nor ,
			String stat_tb,String stat_rb, String searchmode)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(searchmode, cust_id);
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant);  		// Verifying when status is normal
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectrestrictBlock();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_rb ,variant);	
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectrestrictUnblock();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.verifyVDCDetails(debitcard_number,alias_number,stat_nor ,variant); 
	}
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Savings_Account" })
	@Test(description = "Credit Freeze/Unfreeze", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verify_Credit_Freeze_Unfreeze(String active ,String credit_freeze , String debit_freeze, 
			 String total_freeze ,String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
		acman_module.clickActionLink();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(credit_freeze);
		acman_module.clickActionLink();
		acman_module.openActionsDropdown();
		acman_module.selectTotalunfreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
	}
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Savings_Account" })
	@Test(description = "Debit Freeze/Unfreeze", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verify_Debit_Freeze_Unfreeze(String active ,String credit_freeze , String debit_freeze, 
			 String total_freeze ,String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
		acman_module.clickActionLink();	
		acman_module.typeInCommentSection_DebitActions();
		acman_module.openActionsDropdown();
		acman_module.selectDebitFreeze();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(debit_freeze);
		acman_module.clickActionLink();	
		acman_module.openActionsDropdown();
		acman_module.selectTotalunfreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
	}
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=Actions_Savings_Account" })
	@Test(description = "Total Freeze/Unfreeze", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verify_Total_Freeze_Unfreeze(String active ,String credit_freeze , String debit_freeze, 
			 String total_freeze ,String searchmode , String cust_id)
	{
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
		acman_module.clickActionLink();	
		acman_module.openActionsDropdown();
		acman_module.selectTotalFreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(total_freeze);
		acman_module.clickActionLink();	
		acman_module.openActionsDropdown();
		acman_module.selectTotalunfreeze();
		acman_module.typeInCommentSection_DebitActions();
		acman_module.clickOnSubmit_Debit_SavingActions();
		acman_module.clickSavingAccountTab();
		acman_module.verifyingSavingAccountStatus(active);
	}  
	
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=VerifyBasicUserDetails" })
	@Test(description = "VerifyBasicUserDetails", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyBasicUserDetails(String gender ,String dob , String marital_status, String nationality ,String occupation,
			 String empstatus ,String pan_card ,String address ,String doctype , String issuanceCountry , String issuanceplace ,
			 String docnumber , String issuedate , String searchmode , String cust_id)
	{	
		loginModule = new LoginModule();
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		System.out.println(cust_id);
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickMoreDetails();
		acman_module.verifyBasicInformation(gender, dob, marital_status, nationality, occupation, empstatus, pan_card, address);
		acman_module.verifyProofOfIdentity( doctype ,  issuanceCountry ,  issuanceplace , docnumber ,  issuedate);
	}  
	
	
	@DataProviderParams({"fileName=InputData.csv", "tableName=PredeliveryStatus" })
	@Test(description = "Verifying_PredeliveryStatus Debit Card", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifying_PredeliveryStatus(String cust_id,String searchmode , String pre_del_status)
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+cust_id);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		System.out.println(cust_id);
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickDebitCardTab();
		acman_module.verifyPreDelStatus(pre_del_status);
		
		// FIS received remaining
	}  

	@DataProviderParams({"fileName=InputData.csv", "tableName=FixedDepositCheck" })
	@Test(description = "Fixed Deposit Check", dataProviderClass = DataReaderUtil.class, dataProvider = "CsvDataProvider")
	public void verifyFixedDeposutCheck(String cust_id,String searchmode )
	{	
		loginModule = new LoginModule();
		System.out.println("Printing here : "+cust_id);
		loginModule.login("brajendra.srivastava@paytm.com", "test@123");	
		commonModule = new CommonModule();
		acman_module =  new AccountManagement_Module();
		commonModule.navigateToAccountmanagement();
		System.out.println(cust_id);
		commonModule.searchBy(searchmode, cust_id);
		acman_module.clickFixedDepositTab();
		acman_module.verifyFixedDepositTable();
		
		// Add more against close nothing  amount is zero
	}  
}